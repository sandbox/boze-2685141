CKEditor Fonts plugin for Drupal 8
-----------------------------------------
CKEditor Fonts module allows you to to connect your theme fonts to CKEditor.

Install
-------
* Place the ckeditor_fonts directory in the /modules folder of your Drupal installation. 
* Navigate to Manage > Extend. Check the 'Enabled' box next to the 'CKEditor Fonts' and then click the 'Save Configuration' 
button at the bottom.

Configure
---------
* Navigate to Configuration > Text formats and editors > and click Configure Full HTML.
* Drag the Font and Font Size buttons from the "Available buttons" section to the "Active toolbar" section.
* Under "CKEditor plugin settings" click the Font tab.
* In the "CSS" field point to the css file where you have installed these fonts, most likely in your theme. e.g. 
* /themes/mytheme/css/myfonts.css
* Setup the contents of the Font drop down list in the "Select list configuration" field. This configuration has two parts. 
The font label you will see in the editor and the @font-face css rule that will be applied to the text. Enter the select list 
labels and the corresponding @font-face in the format Label/font-face; e.g. 
* Arial/Arial, Helvetica, sans-serif;Comic Sans MS/Comic Sans MS, cursive;Times New Roman /Times New Roman, Times, serif;

