<?php

/**
 * @file
 * Contains \Drupal\ckeditor_fonts\Plugin\CKEditorPlugin\Font.
 */

namespace Drupal\ckeditor_fonts\Plugin\CKEditorPlugin;

use Drupal\ckeditor\CKEditorPluginBase;
use Drupal\ckeditor\CKEditorPluginConfigurableInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\editor\Entity\Editor;

/**
 * Defines the "font" plugin.
 *
 * @CKEditorPlugin(
 *   id = "font",
 *   label = @Translation("Font")
 * )
 */
class Font extends CKEditorPluginBase implements CKEditorPluginConfigurableInterface {

  /**
   * {@inheritdoc}
   */
  public function isInternal() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getFile() {
    return drupal_get_path('module', 'ckeditor_fonts') . '/plugin/font/plugin.js';
  }

  /**
   * {@inheritdoc}
   */
  public function getLibraries(Editor $editor) {
    return array();
  }

  /**
   * {@inheritdoc}
   */
  public function getDependencies(Editor $editor) {
    return array();
  }


  /**
   * {@inheritdoc}
   */
  public function getConfig(Editor $editor) {
    $settings = $editor->getSettings();
    if (!isset($settings['plugins']['font']['selectlist']) && !isset($settings['plugins']['font']['css'])) {
      return array();
    }
    return $settings['plugins']['font'];
  }

  /**
   * {@inheritdoc}
   */
  public function getButtons() {
    $button = function ($name, $direction = 'ltr') {
      $class_name = str_replace(' ', '', $name);
      return array(
        '#type' => 'inline_template',
        '#template' => '<a href="#" class="cke_{{ direction }}" role="button" title="{{ name }}" aria-label="{{ name }}"><span class="ckeditor-button-dropdown cke_button__{{ classname }}_icon">{{ name }}<span class="ckeditor-button-arrow"></span></span></a>',
        '#context' => array(
          'name' => $name,
          'direction' => $direction,
          'classname' => $class_name,
        ),
      );
    };
    return array(
      'Font' => array(
        'label' => t('Font'),
        'image_alternative' => $button('Font'),
        'image_alternative_rtl' => $button('Font', 'rtl'),
      ),
      'FontSize' => array(
        'label' => t('Font Size'),
        'image_alternative' => $button('Font Size'),
        'image_alternative_rtl' => $button('Font Size', 'rtl'),
      ),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state, Editor $editor) {
    $config = array('selectlist' => '', 'css' => '');
    $settings = $editor->getSettings();
    if (isset($settings['plugins']['font'])) {
      $config = $settings['plugins']['font'];
    }

    $form['css'] = array(
      '#title' => t('CSS'),
      '#type' => 'textfield',
      '#default_value' => $config['css'],
      '#description' => t('Path to @font-face CSS file. e.g. /themes/mytheme/css/myfonts.css'),
    );

    $form['selectlist'] = array(
      '#title' => t('Select list configuration'),
      '#type' => 'textarea',
      '#default_value' => $config['selectlist'],
      '#description' => t('Please enter the select list labels and the
      corresponding @font-face in the format Label/font-face; e.g. Arial/Arial,
      Helvetica, sans-serif;Comic Sans MS/Comic Sans MS, cursive;Times New Roman
      /Times New Roman, Times, serif;'),
    );

    return $form;
  }

}
